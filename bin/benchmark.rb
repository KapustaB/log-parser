# frozen_string_literal: true

require 'benchmark'
require_relative '../lib/presenters/pages_presenter'
require_relative '../lib/file_reader/parser'

file = File.open(ARGV[0], 'r')
puts Benchmark.measure {
  FileReader::Parser.new(file).call
}
file.seek(0)
puts "Number of lines parsed: #{file.readlines.length}"
file.close
