# frozen_string_literal: true

require_relative '../lib/file_reader/parser'
require_relative '../lib/validators/file_validator'
require_relative '../lib/presenters/pages_presenter'
require 'colorize'

begin
  Validators::FileValidator.call(ARGV)
rescue Errors::FileParsing => e
  puts e.message.red
  abort
end

file  = File.open(ARGV[0], 'r')
pages = FileReader::Parser.new(file: file).call
file.close
pages_presenter = Presenters::PagesPresenter.new(pages: pages)
pages_presenter.print_unique_visits_desc
pages_presenter.print_visits_desc
