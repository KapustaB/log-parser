# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/path_tracker/page'
require_relative '../../../lib/path_tracker/pages'
require_relative '../../../lib/presenters/pages_presenter'

RSpec.describe Presenters::PagesPresenter do
  before(:all) do
    pages = PathTracker::Pages.new
    @home_page = pages.add_page('/home')
    @home_page.add_visit('255.255.255.5')
    @presenter = Presenters::PagesPresenter.new(pages: pages)
  end

  it 'should print sorted uniq page visits desc' do
    expect do
      @presenter.print_unique_visits_desc
    end.to output("/home                => unique 1 visits\n").to_stdout
  end

  it 'should print sorted page visits asc' do
    expect do
      @presenter.print_visits_desc
    end.to output("/home                => 1 visits\n").to_stdout
  end

  it 'should print page' do
    expect do
      @presenter.send(:print, @home_page, :visits_count)
    end.to output("/home                => 1 visits\n").to_stdout
  end

  it 'should print unique page' do
    expect do
      @presenter.send(:print, @home_page, :unique_visits_count)
    end.to output("/home                => unique 1 visits\n").to_stdout
  end
end
