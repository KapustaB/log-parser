# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/path_tracker/page'

RSpec.describe PathTracker::Page do
  let(:page) { PathTracker::Page.new('/test_path') }

  it 'should creates page' do
    expect(page.visits_count).to eql(0)
    expect(page.uniq_ip_addresses).to eql([])
    expect(page.path).to eql('/test_path')
  end

  it 'should add visit' do
    page.add_visit('255.255.255.1')

    expect(page.visits_count).to eql(1)
    expect(page.uniq_ip_addresses.count).to eql(1)
  end

  it 'should return TRUE if page is visited with existing ip' do
    page.uniq_ip_addresses = ['255.255.255.255']

    expect(page.send(:already_visited?, '255.255.255.255')).to eql(true)
  end

  it 'should return FALSE if page is visited on unique ip' do
    expect(page.send(:already_visited?, '255.255.255.1')).to eql(false)
  end
end
