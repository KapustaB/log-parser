# frozen_string_literal: true


require 'rspec'
require_relative '../../../lib/path_tracker/pages'
require_relative '../../../lib/path_tracker/page'

RSpec.describe PathTracker::Pages do
  let(:pages) { PathTracker::Pages.new }

  it 'should add page to pages' do
    page = pages.add_page('/home')

    expect(pages.pages.first).to eql(page)
  end

  it 'should find page by path' do
    _home_page = pages.add_page('/home')
    index_page = pages.add_page('/index')

    page = pages.find_page_by_path('/index')
    expect(page).to eql(index_page)
  end

  it 'should sort pages by visits descending' do
    home_page  = pages.add_page('/home')  # 0 visits
    index_page = pages.add_page('/index') # 3 visits
    dash_page  = pages.add_page('/dash')  # 2 visits

    index_page.visits_count = 3
    dash_page.visits_count = 2

    expect(pages.sort_by_visits_desc).to eql([index_page, dash_page, home_page])
  end

  it 'should sort pages by unique visits ascending' do
    home_page  = pages.add_page('/home')  # 0 visits
    index_page = pages.add_page('/index') # 3 visits
    dash_page  = pages.add_page('/dash')  # 2 visits

    index_page.unique_visits_count = 3
    dash_page.unique_visits_count = 2

    expect(pages.sort_by_unique_visits_desc).to eql([index_page, dash_page, home_page])
  end

  it 'should return pure page objects' do
    page = pages.add_page('/home')

    expect(pages.pages).to eql([page])
  end
end
