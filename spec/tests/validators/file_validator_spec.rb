# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/validators/file_validator'
require_relative '../../../lib/errors/file_parsing'

RSpec.describe Validators::FileValidator do
  context 'no arguments provided' do
    it 'should throw No argument error' do
      expect do
        Validators::FileValidator.validate_arguments!([])
      end.to raise_error(Errors::FileParsing::NoArgumentError)
    end
  end

  context 'more than one argument is provided' do
    it 'should throw No supported argument error' do
      expect do
        Validators::FileValidator.validate_arguments!(['webserver.log', 'unsupported'])
      end.to raise_error(Errors::FileParsing::NotSupportedArgumentError)
    end
  end

  context 'wrong path is provided' do
    it 'should throw File not found error' do
      expect do
        Validators::FileValidator.validate_file!(['/wrong_file_path'])
      end.to raise_error(Errors::FileParsing::FileNotFoundError)
    end
  end

  context 'file with wrong extension is provided' do
    let(:file_path) { File.expand_path('spec/fixtures/unsupported_extension.txt') }

    it 'should throw Not supported argument error' do
      expect do
        Validators::FileValidator.validate_file!([file_path])
      end.to raise_error(Errors::FileParsing::NotSupportedFileExtensionError)
    end
  end

  context 'empty file is provided' do
    let(:file_path) { File.expand_path('spec/fixtures/empty.log') }

    it 'should throw Empty file error' do
      expect do
        Validators::FileValidator.validate_file!([file_path])
      end.to raise_error(Errors::FileParsing::EmptyFileError)
    end
  end
end
