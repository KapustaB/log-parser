# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/errors/file_parsing'

RSpec.describe Errors::FileParsing do
  context 'NoArgumentError' do
    it 'should output default message' do
      msg = Errors::FileParsing::NoArgumentError.new.message
      expect(msg).to eq('No log file provided.')
    end

    it 'should output given message' do
      msg = Errors::FileParsing::NoArgumentError.new.message('test')
      expect(msg).to eq('test')
    end
  end

  context 'NotSupportedArgumentError' do
    it 'should output default message' do
      msg = Errors::FileParsing::NotSupportedArgumentError.new.message
      expect(msg).to eq('Not supported argument error.')
    end

    it 'should output given message' do
      msg = Errors::FileParsing::NotSupportedArgumentError.new.message('test')
      expect(msg).to eq('test')
    end
  end

  context 'FileNotFoundError' do
    it 'should output default message' do
      msg = Errors::FileParsing::FileNotFoundError.new.message
      expect(msg).to eq('File not found.')
    end

    it 'should output given message' do
      msg = Errors::FileParsing::FileNotFoundError.new.message('test')
      expect(msg).to eq('test')
    end
  end

  context 'NotSupportedFileExtensionError' do
    it 'should output default message' do
      msg = Errors::FileParsing::NotSupportedFileExtensionError.new.message
      expect(msg).to eq('Not supported file extension.')
    end

    it 'should output given message' do
      msg = Errors::FileParsing::NotSupportedFileExtensionError.new.message('test')
      expect(msg).to eq('test')
    end
  end

  context 'EmptyFileError' do
    it 'should output default message' do
      msg = Errors::FileParsing::EmptyFileError.new.message
      expect(msg).to eq('Empty file.')
    end

    it 'should output given message' do
      msg = Errors::FileParsing::EmptyFileError.new.message('test')
      expect(msg).to eq('test')
    end
  end
end
