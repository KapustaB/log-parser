# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/file_reader/parser'

RSpec.describe FileReader::Parser do
  subject { FileReader::Parser.new(file: file).call }

  context 'when valid file is provided' do
    let(:file) { File.open('spec/fixtures/webserver.log', 'r') }

    it 'should parse pages from file' do
      expect(subject.pages.count).to eql(2)
    end
  end

  context 'when invalid file is provided' do
    let(:file) { nil }

    it 'should throw argument error' do
      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
