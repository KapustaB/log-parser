# frozen_string_literal: true

require 'rspec'
require_relative '../../../lib/file_reader/notifier'

class DummyClass
  def call; end
end

RSpec.describe FileReader::Notifier do
  before(:all) do
    @dummy_class = DummyClass.new
    @dummy_class.extend(FileReader::Notifier)
  end

  it 'should output that parsing is started' do
    expect do
      @dummy_class.call
    end.to output(/Parsing file started.../).to_stdout
  end
end
