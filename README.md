# Content of this file
  - [Introduction](#introduction)
  - [Reguirements](#reguirements)
  - [How to run this project](#how-to-run-this-project)

## Introduction

Log parser is a Ruby project that parses log file into visit statistics.
The algorithm counts only unique visits. Uniq visit is defined as a visit from one IP address.

Statistics are printed as: `/example_path => 56 visits`

## Reguirements

Ruby `2.6.5`

## How to run this project

```ruby
 ruby ./bin/log_parser.rb webserver.log
```

Where `webserver.log` is log file with path visits. (included file in project)

Run all tests:
```ruby
 rspec
```

Run specific tests:
```ruby
 rspec /spec/tests/path_tracker/page_spec.rb
```

Run rubocop:
```ruby
 rubocop
```
Run rubocop auto correct all
```ruby
 rubocop -A
```