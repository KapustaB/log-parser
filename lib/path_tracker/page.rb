# frozen_string_literal: true

require 'ipaddr'

module PathTracker
  class Page
    attr_accessor :path, :visits_count, :unique_visits_count, :uniq_ip_addresses

    def initialize(path)
      @path = path
      @unique_visits_count = 0
      @visits_count = 0
      @uniq_ip_addresses = []
    end

    def add_visit(ip_address)
      @visits_count += 1
      return if already_visited?(ip_address)

      uniq_ip_addresses << ip_address # Should be IPAddr.new(ip_address)
      @unique_visits_count += 1
    end

    private

      def already_visited?(ip_address)
        uniq_ip_addresses.include?(ip_address) # Should be IPAddr.new(ip_address)
      end
  end
end
