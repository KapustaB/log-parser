# frozen_string_literal: true

require_relative 'page'

module PathTracker
  class Pages
    def initialize
      @all_pages = {}
    end

    def find_page_by_path(path)
      page = all_pages.slice(path)
      return if page.empty?

      page.first[1]
    end

    def add_page(path)
      all_pages.store(path, Page.new(path))
    end

    def sort_by_visits_desc
      pages.sort_by(&:visits_count).reverse
    end

    def sort_by_unique_visits_desc
      pages.sort_by(&:unique_visits_count).reverse
    end

    def pages
      @pages ||= all_pages.values
    end

    private

      attr_accessor :all_pages
  end
end
