# frozen_string_literal: true

require_relative '../errors/file_parsing'

module Validators
  class FileValidator
    def self.call(args)
      validate_arguments!(args)
      validate_file!(args)
    end

    def self.validate_arguments!(args)
      raise Errors::FileParsing::NoArgumentError            if args.empty?
      raise Errors::FileParsing::NotSupportedArgumentError  if args.count > 1
    end

    def self.validate_file!(args)
      raise Errors::FileParsing::FileNotFoundError               unless File.file?(args[0])
      raise Errors::FileParsing::NotSupportedFileExtensionError  unless File.extname(args[0]) == '.log'
      raise Errors::FileParsing::EmptyFileError                  if File.zero?(args[0])
    end
  end
end
