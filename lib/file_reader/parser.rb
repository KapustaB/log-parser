# frozen_string_literal: true

require_relative '../path_tracker/pages'
require_relative './notifier'
require 'colorize'
module FileReader
  class Parser
    prepend Notifier

    def initialize(file:, pages: PathTracker::Pages.new)
      @file = file
      @pages = pages
    end

    def call
      raise ArgumentError, 'No file to parse.'.red if file.nil?

      parse_file_to_pages
    end

    private

      attr_reader :file, :pages

      def parse_file_to_pages
        file.each_line do |line|
          next if line.chomp.empty?

          path = get_path(line)
          ip_address = get_ip_address(line)
          next if path.nil? || ip_address.nil?

          page = pages.find_page_by_path(path) || pages.add_page(path)
          page.add_visit(ip_address)
        end
        pages
      end

      def get_path(line)
        line[%r{^(?:/[^/\s]+)+}]
      end

      def get_ip_address(line)
        line[/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/]
      end
  end
end
