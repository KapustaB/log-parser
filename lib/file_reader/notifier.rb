# frozen_string_literal: true

require 'colorize'

module FileReader
  module Notifier
    def call
      puts 'Parsing file started...'.blue
      super
    end
  end
end
