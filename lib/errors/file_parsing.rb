# frozen_string_literal: true

module Errors
  module FileParsing
    class Base < StandardError; end

    class NoArgumentError < Base
      def message(msg_text = 'No log file provided.')
        msg_text
      end
    end

    class NotSupportedArgumentError < Base
      def message(msg_text = 'Not supported argument error.')
        msg_text
      end
    end

    class FileNotFoundError < Base
      def message(msg_text = 'File not found.')
        msg_text
      end
    end

    class NotSupportedFileExtensionError < Base
      def message(msg_text = 'Not supported file extension.')
        msg_text
      end
    end

    class EmptyFileError < Base
      def message(msg_text = 'Empty file.')
        msg_text
      end
    end
  end
end
