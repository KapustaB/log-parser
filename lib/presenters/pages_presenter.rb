# frozen_string_literal: true

require 'colorize'

module Presenters
  class PagesPresenter
    def initialize(pages:)
      raise ArgumentError, 'Empty pages'.red if pages.nil?

      @pages = pages
    end

    def print_unique_visits_desc
      pages.sort_by_unique_visits_desc.each { |page| print(page, :unique_visits_count) }
    end

    def print_visits_desc
      pages.sort_by_visits_desc.each { |page| print(page, :visits_count) }
    end

    private

      attr_accessor :pages

      def print(page, attr)
        puts "#{page.path.ljust(20)} =>#{' unique' if attr == :unique_visits_count} #{page.public_send(attr)} visits"
      end
  end
end
